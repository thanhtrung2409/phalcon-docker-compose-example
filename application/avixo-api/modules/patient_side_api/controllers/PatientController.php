<?php
namespace App\PatientApi\Controllers;

class PatientController extends \App\Controllers\ControllerBase
{
    public function index()
    {
        $this->initializeGet();
        $options = $this->buildOptions('name asc', $this->request->get('sort'), $this->request->get('order'), $this->request->get('limit'), $this->request->get('offset'));
        $filters = $this->buildFilters($this->request->get('filter'));
        $cities = $this->findElements('App\PatientApi\Models\Patient', $filters['conditions'], $filters['parameters'], 'id, name', $options['order_by'], $options['offset'], $options['limit']);
        $total = $this->calculateTotalElements('App\PatientApi\Models\Patient', $filters['conditions'], $filters['parameters']);
        $data = $this->buildListingObject($cities, $options['rows'], $total);
        $this->buildSuccessResponse(200, 'common.SUCCESSFUL_REQUEST', $data);
    }

    public function get($id)
    {
        $this->initializeGet();
        $city = $this->findElementById('App\PatientApi\Models\Patient', $id);
        $this->buildSuccessResponse(200, 'common.SUCCESSFUL_REQUEST', $city->toArray());
    }
}
