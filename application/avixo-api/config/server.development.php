<?php

return [
    'database' => [
        'adapter' => 'Mysql', /* Possible Values: Mysql, Postgres, Sqlite */
        'host' => 'localhost',
        'username' => 'root',
        'password' => 'root',
        'dbname' => 'cloud-cms',
        'charset' => 'utf8'
    ],
    'clinic_database' => [
        'adapter' => 'Mysql', /* Possible Values: Mysql, Postgres, Sqlite */
        'host' => 'localhost',
        'username' => 'root',
        'password' => 'root',
        'charset' => 'utf8'
    ],
    // 'log_database' => [
    //     'adapter' => 'Mysql', /* Possible Values: Mysql, Postgres, Sqlite */
    //     'host' => '127.0.0.1',
    //     'username' => 'root',
    //     'password' => 'root',
    //     'dbname' => 'cloud-cms',
    //     'charset' => 'utf8',
    // ],
    'authentication' => [
        'secret' => 'FE6EE23B467C5B97E346426941BFDsds4343aan', // This will sign the token. (still insecure)
        'encryption_key' => '63HfELGI6WE9R6gBqEUnoVaA1J5kXsLS0eJQFScUD3xYon60qEaYLEFSoIuUXV0I', // Secure token with an ultra password
        'expiration_time' => 86400 * 7, // One week till token expires
        'iss' => 'avixo-api', // Token issuer eg. www.myproject.com
        'aud' => 'avixo-api', // Token audience eg. www.myproject.com
    ],
];
