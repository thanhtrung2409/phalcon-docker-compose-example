<?php

/**
 * Registering an autoloader
 */
$loader = new \Phalcon\Loader();

$loader->registerDirs(
    array(
        $config->application->controllersDir,
        $config->application->middlewaresDir,
        $config->application->modelsDir,
        $config->application->patientSideModelsDir,
        $config->application->libraryDir,
    )
)->register();

$loader->registerNamespaces(
    [
       'App\Controllers' => '../controllers/',
       'App\Models'      => '../models/',
       'App\PatientApi\Controllers' => '../modules/patient_side_api/controllers/',
       'App\PatientApi\Models'      => '../modules/patient_side_api/models/',
    ]
)->register();

