<?php
namespace App\Models;

class ClinicModel extends \Phalcon\Mvc\Model
{

	public $clinicDB;

	public function initialize()
    {
    	$di = $this->getDI();
        $clinicDb = [
            'adapter' => $di->get('config')->database->adapter, /* Possible Values: Mysql, Postgres, Sqlite */
            'host' => $di->get('config')->database->host,
            'username' => $di->get('config')->database->username,
            'password' => $di->get('config')->database->password,
            'dbname' => $this->clinicDB,
            'prefix' => 'cms_',
            'charset' => $di->get('config')->database->charset
        ];
        $di->setShared('clinic_database', function () use ($clinicDb) {
            $dbConfig = $clinicDb;
            $adapter = $dbConfig['adapter'];
            unset($dbConfig['adapter']);

            $class = 'Phalcon\Db\Adapter\Pdo\\' . $adapter;

            $connection = new $class($dbConfig);
            $connection->setNestedTransactionsWithSavepoints(true);

            return $connection;
        });
        $this->setConnectionService('clinic_database');
    }
}